var themeBoolean = true;
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "initial";
  document.getElementById('myDiv').setAttribute("class", "row justify-content-center p-4");
}


$(document).ready(function(){

    $("button").click( function() {
        var txt = this.id;
        console.log(txt);
        panggil(txt);
    });

    $("#flip").click(function(){
        $("#panel").slideToggle("slow");
        $("#panel1").slideUp("slow");
        $("#panel2").slideUp("slow");
    });

    $("#flip1").click(function(){
        $("#panel1").slideToggle("slow");
        $("#panel").slideUp("slow");
        $("#panel2").slideUp("slow");
    });

    $("#flip2").click(function(){
        $("#panel2").slideToggle("slow");
        $("#panel1").slideUp("slow");
        $("#panel").slideUp("slow");
    });

    $("#themeButton").click(function(){
        if(themeBoolean === true) {
            $('body').css('background-color', '#2E2E2E');
            themeBoolean = false;
        } else{
            $('body').css('background-color', 'white');
            themeBoolean = true;
        }
    });
    panggil("quilting");
});

function panggil(txt) {
    $.ajax({
        url : "/get-books/" + txt,
        dataType : "json",
        success : function(result) {
            $("tbody").empty()
            for(var i = 0; i < result.length; i++) {

                image = result[i].thumbnail
                title = result[i].title
                authors = result[i].authors
                publisher = result[i].publisher
                $("tbody").append(
                '<tr>\
                <th scope="row">'+ (i+1) +'</th>\
                <td><img src=' + image + '> </td>\
                <td>' + title + '</td>\
                <td>' + authors +  '</td>\
                <td>' + publisher +  '</td>\
                <td ><img class="star" id="img"' + i + ' src="/static/star.png"></td>\
                </tr>'
                )
                $("#img" + i).click(function(){
                    var image = document.getElementById('img' + i);
                    var image1 = "/static/star.png"
                    var image2 = "/static/star1.png"
                    console.log(image.src)
                    if (image.src.match(image1)) {
                        image.src = image2;
                    } else {
                        image.src = image1;
                    }
                });
            }
        }
        
    
    });
}