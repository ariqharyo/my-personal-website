from django.urls import include, path
from .views import *

urlpatterns = [
    path('', main, name='main'),
    path('books', books, name='books'),
    path('get-books/<str:volume>', get_books, name='get-books')
]
