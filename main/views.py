from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.

def main(request):
    return render(request, 'main.html')

def books(request):
    return render(request, 'books.html')

def get_books(request, volume="quilting"):
    data = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + volume)
    dataJSON = data.json()
    books = []
    
    for i in (dataJSON['items']):
        books.append({})
        spec = books[-1]
        try:
            volumeInfo = i["volumeInfo"]
            spec["thumbnail"] = volumeInfo["imageLinks"]["smallThumbnail"]
            spec["title"] = volumeInfo["title"]
            spec["authors"] = volumeInfo["authors"]
            spec["publisher"] = volumeInfo["publisher"]
        except KeyError:
            pass
        
    
    return JsonResponse(books, safe=False)